import EmberObject from '@ember/object';
import AuthGuardMixin from 'mape/mixins/auth-guard';
import { module, test } from 'qunit';

module('Unit | Mixin | auth-guard', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let AuthGuardObject = EmberObject.extend(AuthGuardMixin);
    let subject = AuthGuardObject.create();
    assert.ok(subject);
  });
});
