import EmberObject from '@ember/object';
import AuthSessionMixin from 'mape/mixins/auth-session';
import { module, test } from 'qunit';

module('Unit | Mixin | auth-session', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let AuthSessionObject = EmberObject.extend(AuthSessionMixin);
    let subject = AuthSessionObject.create();
    assert.ok(subject);
  });
});
