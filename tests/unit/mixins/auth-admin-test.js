import EmberObject from '@ember/object';
import AuthAdminMixin from 'mape/mixins/auth-admin';
import { module, test } from 'qunit';

module('Unit | Mixin | auth-admin', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let AuthAdminObject = EmberObject.extend(AuthAdminMixin);
    let subject = AuthAdminObject.create();
    assert.ok(subject);
  });
});
