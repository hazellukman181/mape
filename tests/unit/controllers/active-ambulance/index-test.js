import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Controller | active-ambulance/index', function(hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  test('it exists', function(assert) {
    let controller = this.owner.lookup('controller:active-ambulance/index');
    assert.ok(controller);
  });
});
