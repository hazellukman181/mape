import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Controller | driver/driver', function(hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  test('it exists', function(assert) {
    let controller = this.owner.lookup('controller:driver/driver');
    assert.ok(controller);
  });
});
