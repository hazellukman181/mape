import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | offered', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:offered');
    assert.ok(route);
  });
});
