import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | active-ambulance/index', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:active-ambulance/index');
    assert.ok(route);
  });
});
