import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | active-ambulance/detail', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:active-ambulance/detail');
    assert.ok(route);
  });
});
