import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | provider/driver/new', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:provider/driver/new');
    assert.ok(route);
  });
});
