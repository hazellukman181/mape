import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | provider/index', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:provider/index');
    assert.ok(route);
  });
});
