import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | driver/detail', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:driver/detail');
    assert.ok(route);
  });
});
