import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | driver/after-taste', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:driver/after-taste');
    assert.ok(route);
  });
});
