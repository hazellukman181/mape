import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | user-information/index', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:user-information/index');
    assert.ok(route);
  });
});
