import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | sidebar-provider', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:sidebar-provider');
    assert.ok(route);
  });
});
