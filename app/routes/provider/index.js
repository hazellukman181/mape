import Route from '@ember/routing/route';
import Authenticated from '../../mixins/auth-guard';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';
// import { admin as admin } from "npm:firebase-admin";

export default Route.extend(Authenticated, {  
	model() {
		let currentUser = this.get('session.currentUser.uid');
		return hash({
			provider: this.store.findRecord('provider', currentUser),
			drivers: this.get('store').find('provider', currentUser).then(driver => {
				return driver.get('drivers').then(() => driver); 
			}),
			driverActive: this.get('store').findAll('drivers-available').then(driver => {
				return driver.filterBy('providerId', currentUser)
			}),
			driverInfo: this.get('store').find('provider', currentUser).then(driver => {
				return driver.get('drivers');
			}),
		}) 
	},

	afterModel(models, transition) {
		let currentUser = this.get('session.currentUser.uid');
		this.get('store').find('provider', currentUser)
		.then(() => {
			console.log('all again')
		})
		.catch(error => {
			this.transitionTo('admin.dashboard')
		})
	},

	firebaseApp : service(),

	setupController(controller, models) {
		
		// let resultDriverActive = [];
		// let idDriverInfo = [];
		// idDriverInfo.forEach((value) => {
		// 	let record = this.get('store').findRecord('drivers-available', value)
		// 	.then((data) => {
		// 		resultDriverActive.push(data);
		// 		controller.set('driverActive', resultDriverActive );
		// 		console.log('=====================================result');
		// 		console.log(resultDriverActive);
		// 	})
		// 	.catch((error) => {
		// 		console.log(error.message);
		// 	})
		// });

		// console.log('==================================history');
		// console.log(models.driverActive);

		/**
		*	Mendapatkan info grafis dari driver ambulans
		*	sesuai dengan providernya
		*/
		let drivers = models.drivers.drivers.slice(0, 5);
		let all_drivers = models.drivers.drivers;

		controller.set('provider', models.provider );
		controller.set('drivers', drivers );
		controller.set('all_drivers', all_drivers );

		/**
		*	Mendapatkan info grafis data pelayanan
		*/
		let totalHistory = 0
		models.driverInfo.forEach((value) => {
			totalHistory += value.get('history').length
		})
		controller.set('totalHistory', totalHistory)

		/**
		*	Mendapatkan data info grafis jumlah pelayanan tiap
		*	ambulans sesuai dengan providernya
		*/

		let data = [];
		let labels = [];
		// for (var i = 0; i < models.driverInfo.length; i++) 
		models.driverInfo.forEach((value) => {
			data.push(value.get('history').length);
			labels.push(value.get('name'));
		})
		let dataDriver = {
			datasets: [{
				data: data,
				backgroundColor: [ '#41b311', '#ffa500', '#ea4463', '#44b1e4', '#9c27b0', '#009688', '#ffc107', '#3f51b5', '#795548', '#9c27b0'],
			}],
			labels: labels

		};
		let optionChart = {
			legend: {
				display: false,
			}
		}
		controller.set('chartData', dataDriver)
		controller.set('optionChart', optionChart)

		if(totalHistory == 0) {
			let noData = 'no_data'
			controller.set('noData', noData)
			console.log('==================================================nollllll')
			console.log(noData)
		}

		/**
		*	Mendapatkan info grafis dari driver ambulans yang
		*	sedang aktif sesuai dengan providernya
		*/
		let idDriver = []
		models.driverInfo.forEach(value => {
			idDriver.push({id:value.get('id')})
		})

		let currentUser = this.get('session.currentUser.uid');
		var fire = this.get('firebaseApp')
		var flash = this.get('flashMessages')
		this.get('firebaseApp').database().ref('driversAvailables').on('value', showTotalDataAvailable)
		function showTotalDataAvailable(items){
			var data	= document.getElementById('data');
			var content = '';
			var dadu = [];
			var driver = [];
			items.forEach(child => {
				dadu.push({id: child.key})
			})
			if(items.numChildren() == 0) {
				console.log('nol')
				controller.set('parent', false)
			}
			for (var i = 0; i < items.numChildren(); i++) {
				fire.database().ref('drivers').child(dadu[i].id).on('value', showDataAvailable);
				function showDataAvailable(value) {
					if(value.val().providerId == currentUser) {
						driver.push({name: value.val().name})
					}
					controller.set('parent', driver)
				}
			}
		}

		fire.database().ref('driversAvailables').on('child_added', childAddedProvider)
			function childAddedProvider(items) {
				fire.database().ref('drivers').child(items.key).once('value', function (value) {
					if(value.val().providerId == currentUser) {
						flash.add({
							message: value.val().name + ' mengaktifkan statusnya',
							type: 'info',
							destroyOnClick: true,
							extendedTimeout: 100,
							showProgress: true,
							destroyOnClick: true,
							preventDuplicates: true
						});
					}
				})
			}
		fire.database().ref('driversAvailables').on('child_removed', actionRemovedProvider)
			function actionRemovedProvider(items) {
				fire.database().ref('drivers').child(items.key).once('value', function (value) {
					if(value.val().providerId == currentUser) {
						flash.add({
							message: 'Status pelayanan ' + value.val().name + ' berubah',
							type: 'danger',
							destroyOnClick: true,
							extendedTimeout: 100,
							showProgress: true,
							destroyOnClick: true,
							preventDuplicates: true
							// onDestroy() {
							// 	window.location.reload()
							// }
						});
						// swal({
						// 	title: value.val().name,
						// 	text: "Pengemudi anda berganti status",
						// 	type: "info",
						// 	allowOutsideClick: false,
						// }).then(value => {
						// 	window.location.reload()
						// })
					}
				})
			}
	}
})
