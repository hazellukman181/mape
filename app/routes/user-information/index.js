import Route from '@ember/routing/route';
import { hash } from 'rsvp';
export default Route.extend({
	model() {
		return hash ({
			about: this.get('store').find('page', '1'),
			service: this.get('store').find('page', '2'),
			provider: this.get('store').find('page', '3'),
			driver: this.get('store').find('page', '4'),
			customer: this.get('store').find('page', '5')
		})
	},
	setupController(controller, models) {
		controller.setProperties({
			about: models.about,
			service: models.service,
			provider: models.provider,
			driver: models.driver,
			customer: models.customer
		})
	},
});
