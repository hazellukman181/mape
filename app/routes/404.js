import Route from '@ember/routing/route';

export default Route.extend({
	model() {
		let currentUser = this.get('session.currentUser.uid');
		if(currentUser) {
			return this.store.findRecord('provider', currentUser);
		}
	},

	setupController(controller, model) {
		this._super(controller, model);
		let currentUser = this.get('session.currentUser.uid');
		controller.set('currentUser', currentUser );
	}
});
