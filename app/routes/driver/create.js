import Route from '@ember/routing/route';
import Authenticated from '../../mixins/auth-guard';
import { inject as service } from '@ember/service';
export default Route.extend(Authenticated, {
	model() {
		return this.store.createRecord('driver');
	},

	afterModel(driver, transition) {
		let currentUser = this.get('session.currentUser.uid');
		let provider = this.get('store').find('provider', currentUser).then(provider => {
			if(provider.approval == 'false') {
				this.transitionTo('driver')
				// window.location.href = '/driver'
			}
		})
	},

	setupController(controller, model) {
		this._super(controller, model);
		this.controllerFor('driver.create')
	},

	firebaseApp : service(),

	actions: {
		createDriver(files) {
			var text = "";
			var possible = "0123456789";

			// var fileName = "";
			// var possibleFileName = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

			for (var i = 0; i < 4; i++) {
				text += possible.charAt(Math.floor(Math.random() * possible.length));
			}

			// for (var i = 0; i < 20; i++) {
			// 	fileName += possibleFileName.charAt(Math.floor(Math.random() * possibleFileName.length));
			// }

			let file = document.getElementById('photo').files[0];
			let metadata = {
            'contentType' : file.type
	        };

			let name = this.get('controller').get('model.name');
			let phone = this.get('controller').get('model.phone');
			let email = this.get('controller').get('model.email');
			let address = this.get('controller').get('model.address');
			let platNomor = this.get('controller').get('model.platNomor');
			let providerId = this.get('controller').get('session.currentUser.uid');
			let password = '123456';

			let storageRef = this.get('firebaseApp').storage().ref('profile_images/' + file.name).put(file)
			.then((snapshot) => {
				let uploadUrl = snapshot.downloadURL;
				
				this.get('firebaseApp').auth()
				.createUserWithEmailAndPassword(email, password)
				.then((dataUser) => {
					let currentProvider = this.get('store').peekRecord('provider', providerId);
					let driver = this.get('store').createRecord('driver', {
						id: dataUser.uid,
						providerId: currentProvider, 
						name: name,
						phone: phone,
						address: address,
						platNomor: platNomor,
						code: text,
						email: email,
						created_at: new Date().getTime(),
						profileImageUrl: uploadUrl
					});

					console.log("User created successfully!");
					this.get('firebaseApp').auth().signOut();

					let sweetAlert = this.get('sweetAlert');
					swal({
						  title: 'Konfirmasi penambahan ambulans',
						  html:
						  	'<p class="text-swale">Silahkan masukkan email dan password untuk melakukan proses konfirmasi penambahan ambulans</p>' +
						  	'<label class="swal-label">Alamat email</label>' +
						    '<input id="swal-input1" class="swal2-input">' +
						    '<label class="swal-label">Password</label>' +
						    '<input id="swal-input2" type="password" class="swal2-input">',
						  focusConfirm: false,
						  allowOutsideClick: false,
						  preConfirm: function () {
						    return new Promise(function (resolve) {
						      resolve([
						        $('#swal-input1').val(),
						        $('#swal-input2').val()
						      ])
						    })
						  }
						})
						.then(function (result) {
							let email = result[0];
							let password = result[1];
							let tru = this.get('firebaseApp').auth().signInWithEmailAndPassword(email, password)
							.then(() => {
								currentProvider.get('drivers').pushObject(driver);
								driver.save().then(() => {
									currentProvider.save()
								}).catch((error) => {
									// driver save
									console.log(error.message)
								})
								swal("Pendaftaran berhasil", "Pengemudi ambulans anda berhasil di daftarkan", "success")
								.then(function() {
								  	this.get('router').transitionTo('driver').then(() => {
								  		this.get('flashMessages').success('Data pengemudi ambulans berhasil didaftarkan', {destroyOnClick: true, extendedTimeout: 5000, preventDuplicates: true})
								  	})
								}.bind(this))
							})
							.catch((error) => {
								swal({
									title: 'Pendaftaran gagal',
									html:
									'<p class="text-swale" style="margin: 0 20px;">Ada kesalahan saat proses autentikasi untuk memastikan anda pengguna yang benar, sehingga anda akan di arahkan untuk login kembali</p>' +
									'<small class="text-swale" style="margin-top: 10px;padding: 5px;background: #e2e2e2;border-radius: 5px;display: block;margin: 0 20px;">Error message</small>' +
									'<p style="background: #FF5722;padding: 10px;border-radius: 5px;margin:0 20px;margin-top: 10px;color: white;font-size: 1rem;">' + error.message +'</p>',
									icon: "error",
									focusConfirm: false,
									allowOutsideClick: false,
									})
								.then(function() {
									// window.location.href = this()
									window.location.reload();
									// this.get('target').send('refresh')
									this.get('router').transitionTo('auth.signIn').then(() => {
								  		this.get('flashMessages').danger('Anda salah memasukkan informasi autentikasi saat mendaftarkan pengemudi ambulans, sehingga anda harus masuk kembali dengan akun anda', {destroyOnClick: true, extendedTimeout: 7000, preventDuplicates: true})
								  	})
								}.bind(this))
							})

						}.bind(this)).catch(swal.noop)

				}).catch((error) =>{
					console.log('!!! error saat pendaftaran');
					this.get('flashMessages').danger('Something wrongs! '+ error.message, {destroyOnClick: true, sticky: true, preventDuplicates: true});
				})/*driver sign create*/
 
			}) /*picture snapshot*/

			// storageRef.on('state_changed', null, function(error) {
			// 	console.log('upload failed ' + error.message)
			// }, function() {
			// 	// console.log('Uploaded' + storageRef.snapshot.totalBytes + ' % bytes')
			// 	let uploadUrl = storageRef.snapshot.metadata.downloadURLs[0];

				
			// });

			// let currentProvider = this.get('store').peekRecord('provider', providerId);
			// let driver = this.get('store').createRecord('driver', {
			// 	providerId: currentProvider, 
			// 	name: name,
			// 	phone: phone,
			// 	address: address,
			// 	platNomor: platNomor,
			// 	code: text,
			// 	created_at: new Date().getTime(),
			// 	profileImageUrl: uploadUrl
			// });
			
			// currentProvider.get('drivers').pushObject(driver);
			// driver.save().then(() => {
			// 	currentProvider.save().then(() => {
			// 		this.transitionTo('driver')
			// 	}).catch((error) => {
			// 		console.log('gagal transisi ' + error.message)
			// 	})
			// }).catch((error) => {
			// 	console.log(error.message)
			// })
			// newDriver.save().then(() => this.transitionTo('driver'));
		} /*function driverCreate*/
		
	} /*action brackets*/

});