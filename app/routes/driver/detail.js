import Route from '@ember/routing/route';
import Authenticated from '../../mixins/auth-guard';
export default Route.extend(Authenticated, { 
	model(params) {
		return this.store.findRecord('driver', params.driver_id);
	}, 
});
