import Route from '@ember/routing/route';

export default Route.extend({

	beforeModel() {
	    return this.get('session').fetch().catch(function() {
	    });
	  },

	firebaseApp : Ember.inject.service(),

	actions: {
		signIn() {
			let email    = this.get('controller').get('email');
	        let password = this.get('controller').get('password');
			this.get('firebaseApp').auth().signInWithEmailAndPassword(email, password)
			.then(function() {
		        this.get('router').transitionTo('driver')
		    }.bind(this));
	      // this.get('session').open('firebase', {
	      //   // provider: provider,
	      //   provider: 'password',
	      //   email    : this.get('controller').get('email'),
	      //   password : this.get('controller').get('password'),
	      // }).then(function() {
	      //   this.get('router').transitionTo('driver')
	      // }.bind(this));
	    },
	}
});
