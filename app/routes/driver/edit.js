import Route from '@ember/routing/route';
import Authenticated from '../../mixins/auth-guard';
export default Route.extend(Authenticated, {
	model(params) {
		return this.store.findRecord('driver', params.driver_id);
	},

	firebaseApp : Ember.inject.service(),



	actions: {
		editDriver(driver) {
			// let name = this.get('controller').get('model.name');
			// let phone = this.get('controller').get('model.phone');
			// let email = this.get('controller').get('model.email');
			// let address = this.get('controller').get('model.address');
			// let platNomor = this.get('controller').get('model.platNomor');
			// let providerId = this.get('controller').get('session.currentUser.uid');
			let file = document.getElementById('photo').files[0];
			if (file) {
			let storageRef = this.get('firebaseApp').storage().ref('profile_images/' + file.name).put(file)
			.then((snapshot) => {
				let uploadUrl = snapshot.downloadURL;
				driver.set('profileImageUrl', uploadUrl)
				driver.save().then(() => {
					swal({
					title: "Pengiriman berhasil", 
					text: "edit berhasil", 
					icon: "success",
					buttons: true,
					}).then(() => {
						this.transitionTo('driver')
						this.get('flashMessages').success('Edit data pengemudi ambulans berhasil', {destroyOnClick: true, preventDuplicates: true,extendedTimeout: 500});
					})
				}).catch((error) => {
					this.get('flashMessages').danger('Edit data pengemudi ambulans gagal', {destroyOnClick: true, extendedTimeout: 500, preventDuplicates: true});
				})
			});
			} else {
				driver.save().then(() => {
					this.transitionTo('driver').then(() => {
						this.get('flashMessages').success('Edit data pengemudi ambulans berhasil', {destroyOnClick: true, extendedTimeout: 500, preventDuplicates: true});
					})
				}).catch((error) => {
					this.get('flashMessages').danger('Edit data pengemudi ambulans gagal', {destroyOnClick: true, extendedTimeout: 500, preventDuplicates: true});
				})
			}

		},

		willTransition(transition) {

	      let model = this.controller.get('model');

	      if (model.get('hasDirtyAttributes')) {
	        let confirmation = confirm("Your changes haven't saved yet. Would you like to leave this form?");
	        // swal("Ada perubahan data yang belum anda simpan, apakah anda ingin berpindah halaman?", {
	        // 	button: {
	        // 		cancle: "Tidak, tetap disini",
	        // 		catch: {
	        // 			text: "Lupakan perubahan",
	        // 			value: "leave"
	        // 		}
	        // 	}
	        // }).then((value) => {
	        // 	switch(value) {
	        // 		case "leave":
		       //  		model.rollbackAttributes();
		       //  		swal("Perubahan anda dibuang");
		       //  		break;
		       //  	default:
		       //  		transition.abort();
		       //  		swal("Anda tetap disini");
	        // 	}
	        // });
	        if (confirmation) {
	          model.rollbackAttributes();
	        } else {
	          transition.abort();
	        }
	      }
	    }

	}
});
