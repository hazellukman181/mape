import Route from '@ember/routing/route';
import Authenticated from '../../mixins/auth-guard';
import emailjs from "npm:emailjs-com";
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';

export default Route.extend(Authenticated, {

	model() {
		let currentUser = this.get('session.currentUser.uid');
		return hash({
			drivers: this.get('store').find('provider', currentUser).then(driver => {
				return driver.get('drivers');
			}),
			provider: this.get('store').find('provider', currentUser)
		})
	},
	firebaseApp : service(),
	actions: {
		sendVerificationCode(dataDriver) {
			var templateParams = {
				sender: 'Alpine test email',
				name: dataDriver.name,
				to_email: dataDriver.email,
				activation_code: dataDriver.code
			};

			emailjs.send('gmail', 'template_0gC1is6s', templateParams, 'user_YEAXkGPYQUK1mefUlD97T')
			.then((response) => 
				console.log('SUCCESS!', response.status, response.text),
				swal({
					title: "Pengiriman berhasil", 
					text: "Kode aktivasi " + templateParams.activation_code + " berhasil dikirim ke " + templateParams.to_email, 
					type: "success",
					buttons: true,
				}).then(() => {
					this.get('flashMessages').info('Kode aktivasi telah dikirim, silahkan beritahu pengemudi ambulans', {destroyOnClick: true, extendedTimeout: 5000})
				})
				
			).catch((error) => 
				console.log('oops error', error),
				// this.get('flashMessages').danger('Pengiriman kode aktivasi gagal', {destroyOnClick: true, extendedTimeout: 500})
			)
		}
	}
	
});


// Users
// Customers
// g7gAJempslhSYAYpI2riC3nP87t1
// phone: 
// "+6287724834757"
