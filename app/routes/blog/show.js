import Route from '@ember/routing/route';

export default Route.extend({
	model(params) {
		return this.get('store').findRecord('blog', params.blog_id)
	},
	actions: {
		submitComment(name, body, model) {
			let currentBlog = this.get('store').peekRecord('blog', model.id)
			let comment = this.get('store').createRecord('comment', {
						blogId: currentBlog, 
						name: name,
						body: body,
						createdAt: new Date().getTime(),
					});
			currentBlog.get('comments').pushObject(comment);
			comment.save().then(() => {
				currentBlog.save()
				controller.setProverties({
					name: '',
					body: ''
				})
			}).catch((error) => {
				console.log(error.message)
			})
		},
		applause(model) {
			// this.get('controller').set('isPress', true)
			model.set('applause', model.applause + 1)
			model.save()
		}
	}
});
