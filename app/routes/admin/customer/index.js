import Route from '@ember/routing/route';
import Authenticated from '../../../mixins/auth-guard';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';

export default Route.extend(Authenticated, {
	model() {
		return hash({
			customerInfo: this.get('store').findAll('customer')
		})
	},
	firebaseApp: service(),
	setupController(controller, models) {
		/**
		*	Medapatkan informasi semua pengguna aplikasi
		*/
		controller.set('data', models.customerInfo)
		// var fire = this.get('firebaseApp').database()
		// var data = []
		// fire.ref('customers').on('value', dataCustomers)
		// function dataCustomers(items) {
		// 	items.forEach(child => {
		// 		if (child.val().history == null) {
		// 			console.log('null')
		// 		} else {
		// 			console.log('isi', child.val().history.length)
		// 		}
		// 		data.push({
		// 			name: child.val().name,
		// 			phone: child.val().phone,
		// 			address: child.val().address,
		// 			profileImageUrl: child.val().profileImageUrl,
		// 			demand: child.val().history
		// 		})
		// 		controller.set('data', data)
		// 	})
		// }
	}
});
