import Route from '@ember/routing/route';
import Authenticated from '../../../mixins/auth-guard';
import { hash } from 'rsvp';
export default Route.extend(Authenticated, {
	model(params) {
		return hash({
			customer: this.get('store').findRecord('customer', params.customer_id),
			history: this.get('store').findAll('history')
		})
	},

	setupController(controller, models) {
		/**
		*	Mendapatkan informasi customer
		*/
		controller.set('customer', models.customer)

		/**
		*	Mendapatkan data history customer
		*/
		let idHistory = [];
		let history = [];
		let dataHistory = [];
		models.customer.history.forEach((value) => {
			idHistory.push(value.get('id'))
		});
		models.history.forEach((value) => {
			history.push({
				id: value.get('id'),
				driverId: value.get('driver'),
				timestamp: value.get('timestamp')*1000,
			})
		})
		// for (var i = 0; i < idHistory.length; i++) {
		// 	if(idHistory[i] == history[i].id) {
		// 		dataHistory.push({
		// 			id: idHistory[i],
		// 			timestamp: history[i].timestamp,
		// 			driver: history[i].driverId
		// 		})
		// 	}
		// }
		let result = history.filter(function (item) {
			return idHistory.indexOf(item.id) !== -1
		});

		controller.set('dataHistory', result);
	}
});
