import Route from '@ember/routing/route';
import Authenticated from '../../mixins/auth-guard';

export default Route.extend(Authenticated, {
});
