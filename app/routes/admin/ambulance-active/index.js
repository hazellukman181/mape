import Route from '@ember/routing/route';
import Authenticated from '../../../mixins/auth-guard';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';

export default Route.extend(Authenticated, {
	model() {
		let currentUser = this.get('session.currentUser.uid');
		return hash({
			driverInfo: this.get('store').find('admin', currentUser)
		});
	},
	firebaseApp : service(),
	setupController(controller, models) {
		/**
		*	Mendapatkan info grafis dari driver ambulans yang
		*	sedang aktif sesuai dengan providernya
		*/

		let currentUser = this.get('session.currentUser.uid');
		var fire = this.get('firebaseApp')
		this.get('firebaseApp').database().ref('driversAvailables').on('value', showData);
			function showData(items){
				if (items.val() == null) {
					controller.set('parento', '0')
				}
				var dadu = [];
				var driver = [];
				items.forEach(child => {
					dadu.push({id: child.key,lat: child.val().l[0], lng: child.val().l[1]})
				})
				if(items.numChildren() == 0) {
					controller.set('parent', false)
				}
				for (var i = 0; i < items.numChildren(); i++) {
					fire.database().ref('drivers').child(dadu[i].id).on('value', showDataDrivers);
					function showDataDrivers(value) {
						driver.push({
							name: value.val().name,
							profileImageUrl: value.val().profileImageUrl,
							phone: value.val().phone,
							platNomor: value.val().platNomor,
							lat: dadu[i].lat,
							lng: dadu[i].lng
						})
						controller.set('parent', driver)
					}
				}
				controller.set('child', dadu)
			}

			fire.database().ref('driversAvailables').on('child_added', childAddedIndex)
			function childAddedIndex(items) {
				fire.database().ref('drivers').child(items.key).once('value', function (value) {
					if(value.val().providerId == currentUser) {
						flashMessages.add({
							message: value.val().name + ' mengaktifkan statusnya',
							type: 'info',
							destroyOnClick: true,
							extendedTimeout: 200,
							showProgress: true,
							destroyOnClick: true,
							preventDuplicates: true
						});
					}
				})
			}

			fire.database().ref('driversAvailables').on('child_removed', actionRemovedIndex);
			function actionRemovedIndex(items) {
				fire.database().ref('drivers').child(items.key).once('value', function (value) {
					swal({
						title: value.val().name,
						text: "Pengemudi anda berganti status",
						type: "info",
						allowOutsideClick: false,
					})
				})
			}
	},
});
