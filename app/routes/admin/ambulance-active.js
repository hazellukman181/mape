import Route from '@ember/routing/route';
import Authenticated from '../../mixins/auth-guard';
import { hash } from 'rsvp';
import { inject as service } from '@ember/service';

export default Route.extend(Authenticated, {
	model() {
		let currentUser = this.get('session.currentUser.uid');
		return hash({
			providerInfo: this.get('store').find('admin', currentUser)
		}) 
	},

	firebaseApp : service(),

	setupController(controller, models) {}
});
