import Route from '@ember/routing/route';

export default Route.extend({
	model() {
		return this.get('store').find('page', 'rule')
	},
	actions: {
		isEditing(model) {
			model.set('isEditing', true)
		},
		cancle(model) {
			model.set('isEditing', false)
			this.transitionTo('admin.page.policy.rule')
		},
		saveData(value) {
			this.get('controller').get('model').set('isEditing', false)
			this.get('store').findRecord('page', 'rule').then(rule => {
				rule.set('body', value)
				rule.save()
				swal("Update berhasil", "Data tentang fitur aplikasi telah diperbarui", "success").then(val => {
					this.transitionTo('admin.page.policy.rule')
				})
			})
		}
	}
});
