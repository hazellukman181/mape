import Route from '@ember/routing/route';

export default Route.extend({
	model() {
		return this.get('store').find('page', 'policy')
	},
	actions: {
		isEditing(model) {
			model.set('isEditing', true)
		},
		cancle(model) {
			model.set('isEditing', false)
			this.transitionTo('admin.page.policy.privacy-policy')
		},
		saveData(value) {
			this.get('controller').get('model').set('isEditing', false)
			this.get('store').findRecord('page', 'policy').then(policy => {
				policy.set('body', value)
				policy.save()
				swal("Update berhasil", "Data tentang fitur aplikasi telah diperbarui", "success").then(val => {
					this.transitionTo('admin.page.policy.privacy-policy')
				})
			})
		}
	}
});
