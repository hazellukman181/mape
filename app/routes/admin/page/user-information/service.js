import Route from '@ember/routing/route';
import { hash } from 'rsvp';
export default Route.extend({
	model() {
		return this.get('store').find('page', '2')
	},
	setupController(controller, model) {
		controller.set('about', model)
	},
	actions: {
		cancle(about) {
			this.transitionTo('admin.page.user-information')
		},
		saveData(value) {
			this.get('store').findRecord('page', '2').then(about => {
				about.set('body', value)
				about.save()
				swal("Update berhasil", "Data tentang fitur aplikasi telah diperbarui", "success").then(val => {
					this.transitionTo('admin.page.user-information')
				})
			})
		}
	}
});
