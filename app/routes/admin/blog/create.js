import Route from '@ember/routing/route';
import Authenticated from '../../../mixins/auth-guard';
import { inject as service } from '@ember/service';
export default Route.extend(Authenticated, {
	// model(){
	// 	return this.get('store').createRecord('blog')
	// },
	firebaseApp : service(),
	actions: {
		cancle(value, title) {
			this.transitionTo('admin.blog')
		},
		saveBlog(value, title) {
			var title = this.get('controller').get('title');
			var file = document.getElementById('headerImage').files[0];
			var body = this.get('controller').get('value');
			var currentAdmin = this.get('session.currentUser.uid');
			var fileName = "";
			var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-";
			for (var i = 0; i < 30; i++) {
				fileName += possible.charAt(Math.floor(Math.random() * possible.length));
			}
			var imageName = fileName+'.'+file.name.split('.').pop()
			var slug = title.toString().toLowerCase().replace(/\s+/g, '-').replace(/[^\w\-]+/g, '').replace(/\-\-+/g, '-').replace(/^-+/, '').replace(/-+$/, '')
			var storageRef = this.get('firebaseApp').storage().ref('blog_images/' + imageName).put(file)
			.then(snap => {
				let uploadUrl = snap.downloadURL;
				this.get('firebaseApp').database().ref('blogs')
				.push({
					title: title,
					slug: slug,
					applause: 0,
					body: body,
					headerImage: uploadUrl,
					createdAt: new Date().getTime(),
					updatedAt: new Date().getTime(),
					status: 'Unpublished',
					owner: currentAdmin
				})
				swal("Berhasil", "Blog berhasil ditambahkan", "success")
				.then(function() {
				  	this.get('router').transitionTo('admin.blog').then(() => {
				  		this.get('flashMessages').success('Blog berhasil ditambahkan', {destroyOnClick: true, extendedTimeout: 5000, preventDuplicates: true})
				  	})
				}.bind(this))
			})
		}
	}
});
