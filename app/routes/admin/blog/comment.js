import Route from '@ember/routing/route';
import Authenticated from '../../../mixins/auth-guard';
import { inject as service } from '@ember/service';
export default Route.extend(Authenticated, {
	model(params) {
		return this.get('store').find('blog', params.blog_id)
	},
	firebaseApp : service(),
	setupController(controller, model) {
		controller.set('blog', model)
	},
	actions: {
		deleteComment(commentId) {
			// this.get('firebaseApp').database().ref('comments').child(commentId).on('value', showData);
			// function showData(snap) {
			// 	snap.val().replies
			// }
			this.get('store').findRecord('comment', commentId).then(comment => {
				comment.destroyRecord()
				this.get('flashMessages').info('Komentar berhasil dihapus', {destroyOnClick: true, extendedTimeout: 5000, preventDuplicates: true})
			})
		},
		replyComment(comment) {
			comment.set('isReply', true);
		},
		cancleReplyComment(comment) {
			comment.set('isReply', false);
		},
		postReply(body, commentId, commentary) {
			let currentUser = this.get('session.currentUser.uid');
			let currentAdmin = this.get('store').findRecord('admin', currentUser).then(admin => {

				let currentComment = this.get('store').peekRecord('comment', commentId)
				let reply = this.get('store').createRecord('reply', {
							commentIdId: currentComment, 
							name: admin.name,
							body: body,
							createdAt: new Date().getTime(),
						});
				currentComment.get('replies').pushObject(reply);
				reply.save().then(() => {
					currentComment.save()
					commentary.set('isReply',false)
				}).catch((error) => {
					console.log(error.message)
				})

			})
				this.get('controller').set('isReply', false)
		},
		editReply(reply) {
			reply.set('editReply', true);
		},
		cancelReplyEdit(reply) {
			reply.set('editReply', false);
		},
		saveReplyEdit(reply) {
			reply.save()
			reply.set('editReply', false);	
		},
		deleteReplyEdit(reply) {
			reply.destroyRecord()
		}
	}
});
