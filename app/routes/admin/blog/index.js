import Route from '@ember/routing/route';
import Authenticated from '../../../mixins/auth-guard';
import { hash } from 'rsvp';
import { inject as service } from '@ember/service';
export default Route.extend(Authenticated, {
	model() {
		return hash ({
			blog: this.get('store').findAll('blog')
		})
	},
	setupController(controller, models) {
		controller.set('data', models.blog)
	},
	actions: {
		publishBlog(blogId) {
			this.get('store').findRecord('blog', blogId).then(blog => {
				blog.set('status', 'Published')
				blog.save()
			})
			.then((data) => {
				this.get('flashMessages').info('Blog berhasil dipublish', {destroyOnClick: true, extendedTimeout: 5000, preventDuplicates: true})
			});
		},
		unpublishBlog(blogId) {
			this.get('store').findRecord('blog', blogId).then(blog => {
				blog.set('status', 'Unpublished')
				blog.save()
			})
			.then((data) => {
				this.get('flashMessages').info('Blog berhasil dipublish', {destroyOnClick: true, extendedTimeout: 5000, preventDuplicates: true})
			});
		},
		deleteBlog(blogId) {
			this.get('store').findRecord('blog', blogId).then(blog => {
				blog.destroyRecord()
			})
			.then((data) => {
				this.get('flashMessages').info('Blog berhasil dihapus', {destroyOnClick: true, extendedTimeout: 5000, preventDuplicates: true})
			});
		}
	}
});
