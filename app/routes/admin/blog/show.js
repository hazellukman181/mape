import Route from '@ember/routing/route';
import Authenticated from '../../../mixins/auth-guard';
export default Route.extend(Authenticated, {
	model(params) {
		return this.get('store').findRecord('blog', params.blog_id)
	},
	setupController(controller, model) {
		var currentAdmin = this.get('session.currentUser.uid');
		var dataAdmin = this.get('store').findRecord('admin', currentAdmin)
		controller.set('admin', dataAdmin)
		controller.set('model', model)
	}
});
