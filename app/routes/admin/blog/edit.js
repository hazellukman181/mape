import Route from '@ember/routing/route';
import Authenticated from '../../../mixins/auth-guard';
import { inject as service } from '@ember/service';
export default Route.extend(Authenticated, {
	model(params) {
		return this.get('store').find('blog', params.blog_id)
	},
	firebaseApp : service(),
	actions: {
		cancle(model) {
			this.transitionTo('admin.blog')
		},
		updateBlog(model, value) {
			var file = document.getElementById('headerImage').files[0];
			var slug = model.title.toString().toLowerCase().replace(/\s+/g, '-').replace(/[^\w\-]+/g, '').replace(/\-\-+/g, '-').replace(/^-+/, '').replace(/-+$/, '')
			if(file) {
				var fileName = "";
				var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-";
				for (var i = 0; i < 30; i++) {
					fileName += possible.charAt(Math.floor(Math.random() * possible.length));
				}
				var imageName = fileName+'.'+file.name.split('.').pop()
				var storageRef = this.get('firebaseApp').storage().ref('blog_images/' + imageName).put(file)
				.then(snap => {
					let uploadUrl = snap.downloadURL;
					this.get('firebaseApp').database().ref('blogs').child(model.id)
					.update({
						title: model.title,
						slug: slug,
						body: value,
						headerImage: uploadUrl,
						updatedAt: new Date().getTime(),
					})
					swal("Berhasil", "Blog "+ model.title +" berhasil diubah", "success")
					.then(function() {
					  	this.get('router').transitionTo('admin.blog').then(() => {
					  		this.get('flashMessages').success('Blog berhasil berhasil diubah', {destroyOnClick: true, extendedTimeout: 5000, preventDuplicates: true})
					  	})
					}.bind(this))
				})

			} else {
				this.get('store').findRecord('blog', model.id).then(blog => {
					blog.set('title', model.title)
					blog.set('slug', slug)
					blog.set('body', value)
					blog.set('updatedAt', new Date().getTime())
					blog.save()
				})
				.then((data) => {
					swal("Berhasil", "Blog "+ model.title +" berhasil diubah", "success")
					.then(function() {
					  	this.get('router').transitionTo('admin.blog').then(() => {
					  		this.get('flashMessages').info('Blog berhasil diubah', {destroyOnClick: true, extendedTimeout: 5000, preventDuplicates: true})
					  	})
					}.bind(this))
				});
			}
		}
	}
});
