import Route from '@ember/routing/route';
import isLogin from '../../mixins/auth-admin';
import { inject as service } from '@ember/service';
export default Route.extend(isLogin, {
	model() {
		return this.store.createRecord('admin');
	},

	firebaseApp : service(),

	actions : {
		signIn() {
			let email = this.get('controller').get('model.email');
			let password = this.get('controller').get('model.password')
			this.get('session').open('firebase', {
			// provider: provider,
				provider: 'password',
				email    : email,
				password : password,
			}).then((user) => {
				let admin = this.store.find('admin', user.uid)
				.then((value) => {
					if(value.status == 'admin') {
						this.get('router').transitionTo('admin.dashboard')
					} else {
						this.get('firebaseApp').auth().signOut();
						this.get('flashMessages').danger('Something wrongs! status anda bukan sebagai admin sistem silahkan kunjungi url login yang benar', {destroyOnClick: true, extendedTimeout: 2000});					
						window.setTimeout(function() {
							window.location.reload()
						}, 2000)
					}
				}).catch((error) => {
					this.get('firebaseApp').auth().signOut();
					this.get('flashMessages').danger('Ada kesalahan, email yang anda masukkan tidak dapat kami temukan di data admin', {destroyOnClick: true, extendedTimeout: 2000})
					window.setTimeout(function() {
						window.location.reload()
					}, 2000)
				})
			})
			.catch((error) => {
				this.get('flashMessages').danger('Something wrongs! '+ error.message, {destroyOnClick: true, extendedTimeout: 2000});
			})
		},
	}
});
