import Route from '@ember/routing/route';
import Authenticated from '../../../mixins/auth-guard';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';
export default Route.extend(Authenticated, {
	model() {
		let currentUser = this.get('session.currentUser.uid');
		return hash({
			driverInfo: this.get('store').find('admin', currentUser)
		});
	},
	firebaseApp : service(),
	setupController(controller, models) {
		/**
		*
		*	Mendapatkan data dari driver yang sedang bekerja sesuai dengan
		*	data providernya atau data provider yang login
		*	
		*/
		var fire = this.get('firebaseApp').database()
		let currentUser = this.get('session.currentUser.uid');
		fire.ref('customerRequests').on('value', showDataWorking);
		function showDataWorking(items) {
			var dataDriver = [];
			var detailDriver = [];
			items.forEach(child => {
				dataDriver.push({driverId: child.val().driverId, customerId: child.key})			
			})
			for (var i = 0; i < items.numChildren(); i++) {
				fire.ref('drivers').child(dataDriver[i].driverId).on('value', showData)
				function showData(items) {
					detailDriver.push({
						driverId: dataDriver[i].driverId,
						customerId: dataDriver[i].customerId,
						name: items.val().name,
						phone: items.val().phone,
						platNomor: items.val().platNomor,
						profileImageUrl: items.val().profileImageUrl,
					})
					controller.set('driver', detailDriver)
				}
			}
		}
		fire.ref('driverWorkings').on('child_changed', driverActive)
		function driverActive(items) {
			// fire.ref('drivers').child(items).on('child_added', driverActive)
			// alert('ada driver dalam pelayanan bru')
		}
		
		/**
		*
		*	Mendapatkan data customer request
		*	yang meminta pelayanan ambulans
		*	
		*/
		
	}
});
