import Route from '@ember/routing/route';

export default Route.extend({
	model() {
		let currentUser = this.get('session.currentUser.uid');
		return this.get('store').findRecord('admin', currentUser)
	},

	setupController(controller, model) {
		let date = model.created_at * 1000;
		controller.setProperties({'created_at': date, 'model': model});
	},

	actions: {
		editProfile(model) {
			model.set('isEdit', true);
		},
		cancleUpdate(model){
			model.set('isEdit', false);
		},
		updateProfile(model){
			model.save()
			model.set('isEdit', false)
		}
	}
});
