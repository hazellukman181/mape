import Route from '@ember/routing/route';
import Authenticated from '../../../mixins/auth-guard';
import { inject as service } from '@ember/service';

export default Route.extend(Authenticated, {
	firebaseApp : service(),

	model() {
		let currentUser = this.get('session.currentUser.uid');
		return Ember.RSVP.hash({
			admin: this.get('store').findRecord('admin', currentUser),
			providers: this.get('store').findAll('provider'),
			customers: this.get('store').findAll('customer'),
			drivers: this.get('store').findAll('driver'),
			histories: this.get('store').findAll('history'),
		})
	},

	setupController(controller, models) {
		/**
		*	Mendapatkan info grafis dari provider, ambulans, total pelayanan,dan pengguna
		*/
		controller.setProperties({
			totalProviders: models.providers.length,
			totalCustomers: models.customers.length,
			totalDrivers: models.drivers.length,
			totalHistories: models.histories.length,
		})

		/**
		*	Mendapatkan info grafis jumlah ambulans tiap provider
		*/
		let dataDriver = [];
		let labelDriver = [];

		models.drivers.forEach((value) => {
			dataDriver.push(value.get('history').length);
			labelDriver.push(value.get('name'));
		})
		let chartDataDriver = {
			labels: labelDriver,
			datasets: [{
				data: dataDriver,
				backgroundColor: [ '#009688', '#41b311', '#ea4463', '#ffa500',   '#9c27b0',  '#44b1e4', '#ffc107', '#3f51b5', '#795548', '#9c27b0'],
			}],

		};
		let optionChartDriver = {
			legend: { display: false },
			// title: { display: true, text: 'Data jumlah ambulans tiap instansi', position: 'bottom' },
			scales: { 
				yAxes: [{ 
					ticks: {
						suggestedMin: 0,
						// beginAtZero: true
					}
				}],
				xAxes: [{
					barPercentage: 0.5
				}]
			},
			barThickness: 1
		}
		controller.set('chartDataDriver', chartDataDriver)
		controller.set('optionChartDriver', optionChartDriver)

		/**
		*	Mendapatkan info grafis jumlah ambulans tiap provider
		*/
		let dataProvider = [];
		let labelProvider = [];

		models.providers.forEach((value) => {
			dataProvider.push(value.get('drivers').length);
			labelProvider.push(value.get('name'));
		})
		let chartDataProvider = {
			labels: labelProvider,
			datasets: [{
				data: dataProvider,
				backgroundColor: [ '#44b1e4', '#009688', '#41b311', '#ea4463', '#ffa500',   '#9c27b0',  '#ffc107', '#3f51b5', '#795548', '#9c27b0'],
			}],

		};
		let optionChartProvider = {
			legend: { display: false },
			// title: { display: true, text: 'Data jumlah ambulans tiap instansi', position: 'bottom' },
			scales: { 
				yAxes: [{ 
					ticks: {
						suggestedMin: 0,
						// beginAtZero: true
					}
				}],
				xAxes: [{
					barPercentage: 0.5
				}]
			},
			barThickness: 1
		}
		controller.set('chartDataProvider', chartDataProvider)
		controller.set('optionChartProvider', optionChartProvider)

		/**
		*	Mendapatkan info grafis jumlah pesanan oleh pengguna
		*/
		let dataCustomer = [];
		let labelCustomer = [];

		models.customers.forEach((value) => {
			dataCustomer.push(value.get('history').length);
			labelCustomer.push(value.get('name'));
		})
		let chartDataCustomer = {
			labels: labelCustomer,
			datasets: [{
				data: dataCustomer,
				backgroundColor: [ '#ffc107','#795548', '#009688', '#41b311', '#ea4463', '#ffa500',   '#9c27b0',   '#3f51b5', '#44b1e4', '#9c27b0'],
			}],

		};
		let optionChartCustomer = {
			legend: { display: false },
			// title: { display: true, text: 'Data jumlah ambulans tiap instansi', position: 'bottom' },
		}
		controller.set('chartDataCustomer', chartDataCustomer)
		controller.set('optionChartCustomer', optionChartCustomer)

		console.log('==============================================metadata')
		console.log(this.get('firebaseApp').auth().currentUser.metadata)
	}
});
