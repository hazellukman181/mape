import Route from '@ember/routing/route';
import Authenticated from '../../../mixins/auth-guard';
import { hash } from 'rsvp';
import emailjs from "npm:emailjs-com";
export default Route.extend( {
	
	model() {
		return hash({
			data: this.get('store').findAll('provider'),
			// data2: this.get('store').find('provider', '1')
		})
	},

	setupController(controller, models) {
		controller.set('data', models.data)
	},

	actions: {
		approveProvider(providerId) {
			this.get('store').find('provider', providerId).then(provider => {
				
				provider.set('approval', 'true')
				provider.save()

				var templateParams = {
					sender: 'ALPINE',
					name: provider.name,
					to_email: provider.email
				};

				emailjs.send('gmail', 'approvalprovider', templateParams, 'user_YEAXkGPYQUK1mefUlD97T')
				.then((response) => 
					console.log('SUCCESS!', response.status, response.text),
					swal({
						title: "Pengiriman berhasil", 
						text: "Proses approval berhasil dikirim ke email" + templateParams.to_email, 
						type: "success",
						buttons: true,
					}).then(() => {
						this.get('flashMessages').info('Proses approval instansi' + provider.name + ' berhasil', {destroyOnClick: true, extendedTimeout: 5000})
					})
					
				).catch((error) => 
					console.log('oops error', error),
					// this.get('flashMessages').danger('Pengiriman kode aktivasi gagal', {destroyOnClick: true, extendedTimeout: 500})
				)

			})
		}
	}

});