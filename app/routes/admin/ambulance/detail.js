import Route from '@ember/routing/route';
import Authenticated from '../../../mixins/auth-guard';

export default Route.extend(Authenticated, {
	model(params) {
		return Ember.RSVP.hash({
			driver: this.get('store').findRecord('driver', params.driver_id),
			history: this.get('store').findAll('history')
		})
	},

	setupController(controller, models) {
		/**
		*	Mendapatkan informasi driver
		*/
		controller.set('driver', models.driver)

		/**
		*	Mendapatkan data history driver
		*/
		let idHistory = [];
		let history = [];
		let dataHistory = [];
		models.driver.history.forEach((value) => {
			idHistory.push(value.get('id'))
		});
		models.history.forEach((value) => {
			history.push({
				id: value.get('id'),
				customerId: value.get('customer'),
				timestamp: value.get('timestamp')*1000,
			})
		})

		let result = history.filter(function (item) {
			return idHistory.indexOf(item.id) !== -1
		});
		// for (var i = 0; i < idHistory.length; i++) {
		// 	if(idHistory[i] == history[i].id) {
		// 		dataHistory.push({
		// 			id: idHistory[i],
		// 			timestamp: history[i].timestamp,
		// 			customer: history[i].customerId
		// 		})
		// 	}
		// }


		controller.set('dataHistory', result);
	}

});
