import Route from '@ember/routing/route';
import Authenticated from '../mixins/auth-guard';
import { hash } from 'rsvp';
import { inject as service } from '@ember/service';

export default Route.extend(Authenticated, {
 
	model() {
		let currentUser = this.get('session.currentUser.uid');
		return hash({
			providerInfo: this.get('store').find('provider', currentUser)
		}) 
	},

	firebaseApp : service(),

	setupController(controller, models) {
		controller.set('providerInfo', models.providerInfo)
		/**
		*	Mendapatkan info grafis dari driver ambulans yang
		*	sedang aktif sesuai dengan providernya
		*/
		let currentUser = this.get('session.currentUser.uid');
		var fire = this.get('firebaseApp').database();
		var flash = this.get('flashMessages')
		this.get('firebaseApp').database().ref('driversAvailables').on('value', totalAvailable)
		function totalAvailable(items){
			// var dadu = [];
			// var driver = [];
			// items.forEach(child => {
			// 	dadu.push({id: child.key})
			// })
			// if(items.numChildren() == 0) {
			// 	console.log('nol')
			// 	controller.set('parent', false)
			// }
			// for (var i = 0; i < items.numChildren(); i++) {
			// 	fire.ref('drivers').child(dadu[i].id).on('value', totalDriverAvailable);
			// 	function totalDriverAvailable(value) {
			// 		if(value.val().providerId == currentUser) {
			// 			driver.push({name: value.val().name})
			// 		}
			// 		if (driver.length > 0) {
			// 			controller.set('totalActive', driver)
			// 		} else {
			// 			controller.set('totalActive', false)
			// 		}
			// 	}
			// }
		}

		/**
		*
		*	Mendapatkan data dari driver yang sedang bekerja sesuai dengan
		*	data providernya atau data provider yang login
		*	
		*/
		fire.ref('customerRequests').on('value', totalWorking);
		function totalWorking(items) {
			// var dataDriver = [];
			// var detailDriver = [];
			// items.forEach(child => {
			// 	dataDriver.push({driverId: child.val().driverId, customerId: child.key})			
			// })
			// for (var i = 0; i < items.numChildren(); i++) {
			// 	fire.ref('drivers').child(dataDriver[i].driverId).on('value', totalDriverWorking)
			// 	function totalDriverWorking(items) {
			// 		if(items.val().providerId == currentUser) {
			// 			detailDriver.push({
			// 				driverId: dataDriver[i].driverId
			// 			})
			// 		}
			// 		if (detailDriver.length > 0) {
			// 			controller.set('totalWorking', detailDriver)
			// 		} else {
			// 			controller.set('totalWorking', false)
			// 		}
			// 	}
			// }
		}

	// 	let idDriverInfo = [];
	// 	models.driverInfo.forEach((value) => {
	// 		idDriverInfo.push(value.get('id'));
	// 	});

	// 	for (var i = 0; i < idDriverInfo.length; i++) {
	// 		let available = this.store.query('drivers-available', {equalTo: idDriverInfo[i]})
	// 		.then((value) => {
	// 			console.log('=================================')
	// 			console.log(value.length)
	// 			controller.set('actigo', value);
	// 		})
	// 	}
	// 	/**
	// 	*	Mendapatkan info grafis dari driver ambulans yang
	// 	*	sedang aktif sesuai dengan providernya
	// 	*/
	// 	// let resultDriverActive = [];
	// 	// let idDriverInfo = [];

	// 	// models.driverInfo.forEach((value) => {
	// 	// 	idDriverInfo.push(value.get('id'))
	// 	// });
	// 	// idDriverInfo.forEach((value) => {
	// 	// 	let record = this.get('store').findRecord('drivers-available', value)
	// 	// 	.then((data) => {
	// 	// 		resultDriverActive.push(data);
	// 	// 		controller.set('driverActive', resultDriverActive );
	// 	// 		console.log('=====================================result');
	// 	// 		console.log(resultDriverActive);
	// 	// 	})
	// 	// 	.catch((error) => {
	// 	// 		console.log(error.message);
	// 	// 	})
	// 	// });

	// 	/**
	// 	*	Info provider
	// 	*/
	// 	controller.set('providerInfo', models.providerInfo );

	// 	/**
	// 	*
	// 	*	Mendapatkan data dari driver yang sedang bekerja sesuai dengan
	// 	*	data providernya atau data provider yang login
	// 	*	
	// 	*/
	// 	console.log('=================================driverInfo')
	// 	console.log(models.driverInfo.length)

	// 	let resultDataDriverWorking = [];
	// 	let resultWorking = [];
	// 	let idDriverWorking = [];
	// 	if (models.driverInfo.length > 1) {

	// 	models.driverInfo.forEach((value) => {
	// 		idDriverWorking.push({
	// 			id: value.get('id'),
	// 			name: value.get('name'),
	// 			platNomor: value.get('platNomor'),
	// 			phone: value.get('phone'),
	// 			profileImageUrl: value.get('profileImageUrl'),
	// 		});
	// 	});
	// 	for(var i=0; i < idDriverWorking.length; i++){
	// 		let record = this.get('store').peekRecord('drivers-working', idDriverWorking[i].id)
	// 		console.log('======================================id driver');
	// 		console.log(record)
	// 		if(record !== null){
	// 			resultDataDriverWorking.push({
	// 				driverId: record.get('id'),
	// 				dataDriver: {
	// 					name: idDriverWorking[i].name,
	// 					platNomor: idDriverWorking[i].platNomor,
	// 					phone: idDriverWorking[i].phone,
	// 					profileImageUrl: idDriverWorking[i].profileImageUrl,
	// 					lat: record.get('l').get('0'),
	// 					lng: record.get('l').get('1')
	// 				}
	// 			})
	// 			resultWorking.push(record)
	// 			console.log('=====================================data driver')
	// 			console.log(resultDataDriverWorking)
	// 		} else {
	// 			console.log('=====================================ada driver mangkir')
	// 		}
	// 	}

	// 	} /* jika terdapat record driverInfo */
	// 	/**
	// 	*
	// 	*	Mendapatkan data customer request
	// 	*	yang meminta pelayanan ambulans
	// 	*	
	// 	*/
	// 	let idCustomerRequest = [];
	// 	let dataCustomerRequest = [];
	// 	let dataRequest = [];

	// 	if (models.driverInfo.length > 1) {

	// 	models.customerRequest.forEach((value) => {
	// 		idCustomerRequest.push({
	// 			id: value.get('id'),
	// 			driverId: value.get('driverId'),
	// 			lat: value.get('l').get('0'),
	// 			lng: value.get('l').get('0')
	// 		});
	// 	})
	// 	for (var i = 0; i < idCustomerRequest.length; i++) {
	// 		if(idCustomerRequest[i].driverId == resultDataDriverWorking[i].driverId) {
	// 			dataRequest.push({
	// 				driverId: idCustomerRequest[i].driverId,
	// 				customerId: idCustomerRequest[i].id,
	// 				dataDriver: {
	// 					name: resultDataDriverWorking[i].dataDriver.name,
	// 					platNomor: resultDataDriverWorking[i].dataDriver.platNomor,
	// 					phone: resultDataDriverWorking[i].dataDriver.phone,
	// 					profileImageUrl: resultDataDriverWorking[i].dataDriver.profileImageUrl,
	// 					lat: resultDataDriverWorking[i].dataDriver.lat,
	// 					lng: resultDataDriverWorking[i].dataDriver.lng
	// 				},
	// 				dataCustomer: {
	// 					lat: idCustomerRequest[i].lat,
	// 					lng: idCustomerRequest[i].lng
	// 				}
	// 			})
	// 		} else {
	// 			console.log('===================================== TIDAK');
	// 		}
	// 	}

	// 	} /*jika ada driver info*/

	// 	controller.set('dataRequest', dataRequest );
	// 	console.log('===================================== ADA');
	// 	console.log(dataRequest)

	},

});
