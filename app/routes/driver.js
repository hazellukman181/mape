import Route from '@ember/routing/route';
import Authenticated from '../mixins/auth-guard';
import { hash } from 'rsvp';
import { inject as service } from '@ember/service';

export default Route.extend(Authenticated, {

	model() {
		let currentUser = this.get('session.currentUser.uid');
		return hash({
			providerInfo: this.get('store').find('provider', currentUser)
		}) 
	},

	firebaseApp : service(),

	setupController(controller, models) {
		/**
		*	Mendapatkan informasi provider aktif
		*/
		controller.set('providerInfo', models.providerInfo)

		/**
		*	Mendapatkan info grafis dari driver ambulans yang
		*	sedang aktif sesuai dengan providernya
		*/
		let currentUser = this.get('session.currentUser.uid');
		var fire = this.get('firebaseApp').database()
		var flash = this.get('flashMessages')
		this.get('firebaseApp').database().ref('driversAvailables').on('value', totalAvailable)
		function totalAvailable(items){
			// var dadu = [];
			// var driver = [];
			// items.forEach(child => {
			// 	dadu.push({id: child.key})
			// })
			// if(items.numChildren() == 0) {
			// 	console.log('nol')
			// 	controller.set('parent', false)
			// }
			// for (var i = 0; i < items.numChildren(); i++) {
			// 	fire.ref('drivers').child(dadu[i].id).on('value', totalDriverAvailable);
			// 	function totalDriverAvailable(value) {
			// 		if(value.val().providerId == currentUser) {
			// 			driver.push({name: value.val().name})
			// 		}
			// 		if (driver.length > 0) {
			// 			controller.set('totalActive', driver)
			// 		} else {
			// 			controller.set('totalActive', false)
			// 		}
			// 	}
			// }
		}

		/**
		*
		*	Mendapatkan data dari driver yang sedang bekerja sesuai dengan
		*	data providernya atau data provider yang login
		*	
		*/
		fire.ref('customerRequests').on('value', totalWorking);
		function totalWorking(items) {
			// var dataDriver = [];
			// var detailDriver = [];
			// items.forEach(child => {
			// 	dataDriver.push({driverId: child.val().driverId, customerId: child.key})			
			// })
			// for (var i = 0; i < items.numChildren(); i++) {
			// 	fire.ref('drivers').child(dataDriver[i].driverId).on('value', totalDriverWorking)
			// 	function totalDriverWorking(items) {
			// 		if(items.val().providerId == currentUser) {
			// 			detailDriver.push({
			// 				driverId: dataDriver[i].driverId
			// 			})
			// 		}
			// 		if (detailDriver.length > 0) {
			// 			controller.set('totalWorking', detailDriver)
			// 		} else {
			// 			controller.set('totalWorking', false)
			// 		}
			// 	}
			// }
		}

	},

});
