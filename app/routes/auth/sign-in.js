import Route from '@ember/routing/route';
import isLogin from '../../mixins/auth-session';
import { inject as service } from '@ember/service';
export default Route.extend(isLogin, {

	model() {
		return this.store.createRecord('provider');
	},


	firebaseApp : service(),

	actions : {
		signIn() {
			let email = this.get('controller').get('model.email');
			let password = this.get('controller').get('model.password')
			this.get('session').open('firebase', {
				// provider: provider,
				provider: 'password',
				email    : email,
				password : password,
			}).then((user) => {
				let provider = this.store.find('provider', user.uid)
				.then((value) => {
					if(value.status == 'provider') {
						this.get('router').transitionTo('provider')
					} else {
						this.get('firebaseApp').auth().signOut();
						this.get('flashMessages').danger('Something wrongs! status anda sebagai admin sistem silahkan kunjungi url login yang benar', {destroyOnClick: true, extendedTimeout: 2000});					
						this.get('router').transitionTo('auth.signIn');
					}
				}).catch(() => {
					this.get('firebaseApp').auth().signOut();
					this.get('flashMessages').danger('Ada kesalahan, email yang anda masukkan tidak dapat kami temukan di data instansi', {destroyOnClick: true, extendedTimeout: 5000});
					this.get('router').transitionTo('auth.signIn');
				})
			})
			.catch((error) => {
				this.get('flashMessages').danger('Something wrongs! '+ error.message, {destroyOnClick: true, extendedTimeout: 2000});
			})
		},
	}
});
