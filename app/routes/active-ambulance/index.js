import Route from '@ember/routing/route';
import Authenticated from '../../mixins/auth-guard';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';

export default Route.extend(Authenticated, {
	model() {
		let currentUser = this.get('session.currentUser.uid');
		return hash({
			driverInfo: this.get('store').find('provider', currentUser).then(driver => {
				return driver.get('drivers');
			})
		});
	},
	firebaseApp : service(),
	setupController(controller, models) {
		/**
		*	Mendapatkan info grafis dari driver ambulans yang
		*	sedang aktif sesuai dengan providernya
		*/
		let idDriver = []
		models.driverInfo.forEach(value => {
			idDriver.push({id:value.get('id')})
		})

		let currentUser = this.get('session.currentUser.uid');
		var fire = this.get('firebaseApp')
		this.get('firebaseApp').database().ref('driversAvailables').on('value', showData);
			function showData(items){
				if (items.val() == null) {
					controller.set('parento', '0')
				}
				var dadu = [];
				var driver = [];
				items.forEach(child => {
					dadu.push({id: child.key,lat: child.val().l[0], lng: child.val().l[1]})
				})
				if(items.numChildren() == 0) {
					controller.set('parent', false)
				}
				for (var i = 0; i < items.numChildren(); i++) {
					fire.database().ref('drivers').child(dadu[i].id).on('value', showDataDrivers);
					function showDataDrivers(value) {
						if(value.val().providerId == currentUser) {
							driver.push({
								name: value.val().name,
								profileImageUrl: value.val().profileImageUrl,
								phone: value.val().phone,
								platNomor: value.val().platNomor,
								lat: dadu[i].lat,
								lng: dadu[i].lng
							})
						}
							controller.set('parent', driver)
					}
				}
				controller.set('child', dadu)
			}

			fire.database().ref('driversAvailables').on('child_added', childAddedIndex)
			function childAddedIndex(items) {
				fire.database().ref('drivers').child(items.key).once('value', function (value) {
					if(value.val().providerId == currentUser) {
						// flashMessages.add({
						// 	message: value.val().name + ' mengaktifkan statusnya',
						// 	type: 'info',
						// 	destroyOnClick: true,
						// 	extendedTimeout: 200,
						// 	showProgress: true,
						// 	destroyOnClick: true,
						// 	preventDuplicates: true
						// });
					}
				})
			}

			fire.database().ref('driversAvailables').on('child_removed', actionRemovedIndex);
			function actionRemovedIndex(items) {
				fire.database().ref('drivers').child(items.key).once('value', function (value) {
					if(value.val().providerId == currentUser) {
						swal({
							title: value.val().name,
							text: "Pengemudi anda berganti status",
							type: "info",
							allowOutsideClick: false,
						})
					}
				})
			}
	},

	actions: {
		onClick(){

		}
	}
});
// 		if(record.get('providerId.id') == currentUser) {
		// 			resultDataDriverActiveNew.push(record)
		// 		}
		// 		allData.push({
		// 			idAvailable: resultDataDriverActive[i],
		// 			name: record.get('name'),
		// 			phone: record.get('phone'),
		// 			platNomor: record.get('platNomor'),
		// 			profileImageUrl: record.get('profileImageUrl'),
		// 			location: resultActive[i]
		// 		})
		// }

		// this.controllerFor('active-ambulance.index')
		// /**
		// *	Deklarasi variable
		// */		
		// let currentUser = this.get('session.currentUser.uid');
		// let lightMapStyle = [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]}];

		// /**
		// *	Mendapatkan detail dari driver yang sedang aktif
		// *	var :
		// *		resultDriverActive, id
		// */
		// let resultDriverActive = [];
		// let idDriverInfo = [];

		// console.log('=========================driver 00')
		// console.log(models.driverInfo)

		// models.driverInfo.forEach((value) => {
		// 	idDriverInfo.push(value.get('id'));
		// });
		// idDriverInfo.forEach((value) => {
		// 	let record = this.get('store').findRecord('drivers-available', value)
		// 	.then((data) => {
		// 		resultDriverActive.push(data);
		// 		controller.set('driverActive', resultDriverActive );
		// 		console.log('=====================================result');
		// 		console.log(resultDriverActive);
		// 	})
		// 	.catch((error) => {
		// 		console.log('=====================================no result');
		// 		console.log(error.message);
		// 	})
		// });

		// *
		// *
		// *	Mendapatkan data dari driver yang sedang aktif sesuai
		// *	data providernya atau data provider yang login
		// *	
		
		// let resultDataDriverActive = [];
		// let resultActive = [];
		// let resultDataDriverActiveNew = [];
		// let idDriverActive = [];
		// let allData = [];
		// models.driverInfo.forEach((value) => {
		// 	idDriverActive.push(value.get('id'));
		// 	// console.log('======================================id driver');
		// 	// console.log(value.get('id'))
		// });
		// for(var i=0; i < idDriverActive.length; i++){
		// 	let record = this.get('store').peekRecord('drivers-available', idDriverActive[i])
		// 	console.log('======================================id driver');
		// 	console.log(record)
		// 	if(record !== null){
		// 		resultDataDriverActive.push(record.get('id'))
		// 		resultActive.push(record)
		// 	} else {
		// 		console.log('=====================================ada driver mangkir')
		// 	}
		// }
		// for(var i=0; i < resultDataDriverActive.length; i++){
		// 	let record = this.get('store').peekRecord('driver', resultDataDriverActive[i])
		// 		if(record.get('providerId.id') == currentUser) {
		// 			resultDataDriverActiveNew.push(record)
		// 		}
		// 		allData.push({
		// 			idAvailable: resultDataDriverActive[i],
		// 			name: record.get('name'),
		// 			phone: record.get('phone'),
		// 			platNomor: record.get('platNomor'),
		// 			profileImageUrl: record.get('profileImageUrl'),
		// 			location: resultActive[i]
		// 		})
		// }
		// console.log('=====================================result data driver');
		// console.log(resultDataDriverActive)

		// controller.set('dataDriver', resultDataDriverActiveNew);
		// controller.set('resultActive', resultActive);
		// controller.set('allData', allData);

		// controller.set('lightMapStyle', lightMapStyle );
		// controller.set('currentUser', currentUser );

		

	// let drivers = this.get('firebaseApp').database().ref('/driversavailables').on('value', function(snapshot) {
	// 	controller.set('active', snapshot.val() );
	// 	console.log(snapshot.val())
	// }, function(error) {
	// 	console.log('!!! ADA ERROR');
	// 	console.log(error.message);
	// })