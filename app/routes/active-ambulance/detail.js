import Route from '@ember/routing/route';
import Authenticated from '../../mixins/auth-guard';
export default Route.extend(Authenticated, { 
	model(params) {
		return Ember.RSVP.hash({
			map: this.store.findRecord('drivers-available', params.driver_id),
			driver: this.get('store').findRecord('driver', params.driver_id)
		});
		// let map = this.store.findRecord('drivers-available', params.driver_id);
		
		// console.log(map)
	},

	gMap: Ember.inject.service(), 

	setupController(controller, models) {
		this._super(controller, models);
		this.controllerFor('active-ambulance.detail');
		let lightMapStyle = [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]}];
		controller.set('lightMapStyle', lightMapStyle );
		controller.set('lat', models.map.l[0]);
		controller.set('lng', models.map.l[1]);
		controller.set('driver', models.driver);

		console.log('======================================marker');
		console.log(models.map.l[0]);
		console.log(models.map.l[1]);

		let lat = models.map.l[0];
		let lng = models.map.l[1];

		this.get('gMap')
	      .geocode({lat, lng})
	      .then((geocodes) => {
	        // Get first geocode address
	        controller.set('address', geocodes[0].formatted_address);
	        console.log('====================================geocode');
	        console.log(geocodes[0].formatted_address);
	      })
	      .catch((err) => console.error(err));

	},

	actions: {
		recenterMap(map,lat,lng) {
		    map.setZoom(17);
		    map.panTo({ lat, lng });
	    },
	    onClick() {
	    	console.log("clocke");
	    }
	}
});
