import Route from '@ember/routing/route';
import Authenticated from '../../mixins/auth-guard';
export default Route.extend(Authenticated, {
	model(){
		let currentUser = this.get('session.currentUser.uid');
		return Ember.RSVP.hash({
			provider: this.store.findRecord('provider', currentUser),
			drivers: this.get('store').find('provider', currentUser).then(driver => {
				return driver.get('drivers').then(() => driver); 
			}),
			driverActive: this.get('store').findAll('drivers-available').then(driver => {
				return driver.filterBy('providerId', currentUser)
			}),
			driverInfo: this.get('store').find('provider', 'pJEqZ2Uc1FQvYjxIVP5mWlTfuTD3').then(driver => {
			    return driver.get('drivers');
			})
		});
	},

	firebaseApp : Ember.inject.service(),

	setupController(controller, models) {
		let resultDriverActive = [];
		let idDriverInfo = [];
		models.driverInfo.forEach((value) => {
			idDriverInfo.push(value.get('id'));
		});

		for (var i = 0; i < idDriverInfo.length; i++) {
			let available = this.store.query('drivers-available', {equalTo: idDriverInfo[i]})
			.then((value) => {
				console.log('=================================')
				console.log(value.length)
				controller.set('actigo', value);
			})
		}
	}
});
