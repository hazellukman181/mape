import Route from '@ember/routing/route';
import { hash } from 'rsvp';
import { inject as service } from '@ember/service';
import Authenticated from '../../mixins/auth-guard';

export default Route.extend(Authenticated, {
	model(params) {
		return hash({
			customerRequest: this.store.find('customer-request', params.customer_id),
		})
	},

	firebaseApp : service(),
	gMap: service(), 

	setupController(controller, models) {

		var fire = this.get('firebaseApp').database()
		var gMap = this.get('gMap')
		/**
		*	Mendapatkan data customer yang melakukan request
		*	yang didapat melalui record `customerRequest`
		*/
		fire.ref('customers').child(models.customerRequest.id).on('value', customerDetail)
		function customerDetail(child) {
			controller.setProperties({
				customerName: child.val().name,
				customerPhone: child.val().phone, 
				customerProfilImageUrl: child.val().profileImageUrl
			})
		}
		/**
		*	Mendapatkan data posisi customer yang melakukan request
		*/
		fire.ref('customerRequests').child(models.customerRequest.id).on('value', customerLocation)
		function customerLocation(child) {
			var customerLoc = { lat: parseFloat(child.val().l[0]), lng: parseFloat(child.val().l[1]) };
			let lat = child.val().l[0];
			let lng = child.val().l[1];
			gMap.geocode({lat, lng})
			.then((geocodes) => {
				controller.set('pickupLocation', geocodes[0].formatted_address);
				// console.log('====================================geocode');
				// console.log(geocodes[0].formatted_address);
			}).catch((error) => {
				console.log('=======================oops geocode')
				console.log(error)
			})
			console.log('customer location', customerLoc)
			controller.setProperties({
				customerLat: child.val().l[0],
				customerLng: child.val().l[1],
				customer: customerLoc
			})
		}
		/**
		*	Mendapatkan data lokasi pengemudi
		*/
		fire.ref('driversWorkings').child(models.customerRequest.driverId).on('value', driverLocation)
		function driverLocation(child) {
			const driverLoc = { lat: parseFloat(child.val().l[0]), lng: parseFloat(child.val().l[1]) };
			controller.setProperties({
				driverLat: child.val().l[0],
				driverLng: child.val().l[1],
				driver: driverLoc
			})
		}
		/**
		*	Mendapatkan informasi lengkap pengemudi
		*/
		fire.ref('drivers').child(models.customerRequest.driverId).on('value', driverDetail)
		function driverDetail(child) {
			controller.setProperties({
				driverName: child.val().name,
				driverPhone: child.val().phone,
				driverPlat: child.val().platNomor, 
				driverProfileImageUrl: child.val().profileImageUrl
			})
		}
		/**
		*	Aksi ketika pelayanan selesai
		*/
		fire.ref('customerRequests').child(models.customerRequest.id).on('child_removed', customerDone)
		function customerDone(child) {
			fire.ref('drivers').child(child.val()).on('value', value => {
				swal({
						title: value.val().name,
						text: "Proses pelayanan ambulans selesai",
						type: "info",
						allowOutsideClick: false,
					}).then(() => {
						window.location.href = '/provider'
					})
				// alert('Proses pelayanan driver '+ value.val().name +' telah selesai')
			})
		}

		// Styling google maps
		let lightMapStyle = [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]}];
		controller.set('lightMapStyle', lightMapStyle)
	}
});

		// /**
		// *	Mendapatkan data perjalanan ambulans dari id customer
		// *	mengembalikan data perjalanan
		// */
		// let lightMapStyle = [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]}];
		// let dataDriverWorking = [];
		// let dataDriver = [];
		// let dataCustomer = [];

		// /**
		// *	Mendapatkan data customer yang melakukan request
		// *	yang didapat melalui record `customerRequest`
		// */
		// let idCustomer = models.customerRequest.id;
		// let record = this.get('store').findRecord('customer', idCustomer)
		// .then((value) => {

		// 	dataCustomer.push({
		// 		name: value.get('name'),
		// 		phone: value.get('phone'),
		// 		address: value.get('address'),
		// 		profileImageUrl: value.get('profileImageUrl'),
		// 		location: {
		// 			lat: models.customerRequest.l[0],
		// 			lng: models.customerRequest.l[1]
		// 		}
		// 	})

		// 	console.log('=========================================data customer');
		// 	console.log(dataCustomer);
		// 	controller.set('dataCustomer', dataCustomer)
		// })
		// .then(() => {
		// 	/**
		// 	*	Mencari data driver yang sedang dalam
		// 	*	pelayanan kepada pelanggan
		// 	*/
		// 	let recordDriver = this.get('store').find('drivers-working', models.customerRequest.driverId)
		// 	.then((value) => {
		// 		dataDriverWorking.push({
		// 			lat: value.get('l').get('0'),
		// 			lng: value.get('l').get('1')
		// 		})
		// 			/**
		// 			*	Mencari data driver di record `drivers`
		// 			*	berdasarkan `driverId` pada `customerRequest`
		// 			*/
		// 			this.get('store').find('driver', models.customerRequest.driverId)
		// 			.then((value) => {
		// 				dataDriver.push({
		// 					name: value.get('name'),
		// 					phone: value.get('phone'),
		// 					address: value.get('address'),
		// 					platNomor: value.get('platNomor'),
		// 					profileImageUrl: value.get('profileImageUrl'),
		// 					location: {
		// 						lat: dataDriverWorking[0].lat,
		// 						lng: dataDriverWorking[0].lng
		// 					}
		// 				})

		// 				const driverLoc = { lat: parseFloat(dataDriver[0].location.lat), lng: parseFloat(dataDriver[0].location.lng) };
		// 				const customerLoc = { lat: parseFloat(dataCustomer[0].location.lat), lng: parseFloat(dataCustomer[0].location.lng) };
		// 				console.log("======================================driver lat");
		// 				console.log(driverLoc, customerLoc);

		// 				console.log('===============================================data driver')
		// 				console.log(dataDriver[0].name);

		// 				controller.setProperties({
		// 					dataDriver: dataDriver,
		// 					driver: driverLoc,
		// 					customer: customerLoc,
		// 					lightMapStyle: lightMapStyle
		// 				})
		// 			})
		// 			.catch((error) => {
		// 				console.log(error.message)
		// 			})
		// 	})
		// 	.catch((error) => {
		// 		console.log(error.message)
		// 	})
		// })
		// .catch((error) => {
		// 	console.log(error.message)
		// })

		// /**
		// *	Mendapatkan nama alamat berdasarkan latitude dan longitude
		// *	dari data tujuan
		// */
		// let lat = models.customerRequest.l[0];
		// let lng = models.customerRequest.l[1];	
