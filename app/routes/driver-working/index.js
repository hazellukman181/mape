import Route from '@ember/routing/route';
import Authenticated from '../../mixins/auth-guard';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';
export default Route.extend(Authenticated, {
	model() {
		let currentUser = this.get('session.currentUser.uid');
		return hash({
			driverInfo: this.get('store').find('provider', currentUser).then(driver => {
			    return driver.get('drivers');
			})
		});
	},
	firebaseApp : service(),
	setupController(controller, models) {
		/**
		*
		*	Mendapatkan data dari driver yang sedang bekerja sesuai dengan
		*	data providernya atau data provider yang login
		*	
		*/
		var fire = this.get('firebaseApp').database()
		let currentUser = this.get('session.currentUser.uid');
		fire.ref('customerRequests').on('value', showDataWorking);
		function showDataWorking(items) {
			var dataDriver = [];
			var detailDriver = [];
			items.forEach(child => {
				dataDriver.push({driverId: child.val().driverId, customerId: child.key})			
			})
			for (var i = 0; i < items.numChildren(); i++) {
				fire.ref('drivers').child(dataDriver[i].driverId).on('value', showData)
				function showData(items) {
					if(items.val().providerId == currentUser) {
						detailDriver.push({
							driverId: dataDriver[i].driverId,
							customerId: dataDriver[i].customerId,
							name: items.val().name,
							phone: items.val().phone,
							platNomor: items.val().platNomor,
							profileImageUrl: items.val().profileImageUrl,
						})
					}
					controller.set('driver', detailDriver)
				}
			}
		}
		fire.ref('driverWorkings').on('child_changed', driverActive)
		function driverActive(items) {
			console.log('ambulans sedang melaju')
			// fire.ref('drivers').child(items).on('child_added', driverActive)
			// alert('ada driver dalam pelayanan bru')
		}
		
		/**
		*
		*	Mendapatkan data customer request
		*	yang meminta pelayanan ambulans
		*	
		*/
		
	}
});

// console.log('=================================driverInfo')
// 		console.log(models.driverInfo.length)

// 		let resultDataDriverWorking = [];
// 		let resultWorking = [];
// 		let idDriverWorking = [];
// 		if (models.driverInfo.length > 1) {

// 		models.driverInfo.forEach((value) => {
// 			idDriverWorking.push({
// 				id: value.get('id'),
// 				name: value.get('name'),
// 				platNomor: value.get('platNomor'),
// 				phone: value.get('phone'),
// 				profileImageUrl: value.get('profileImageUrl'),
// 			});
// 		});
// 		for(var i=0; i < idDriverWorking.length; i++){
// 			let record = this.get('store').peekRecord('drivers-working', idDriverWorking[i].id)
// 			console.log('======================================id driver');
// 			console.log(record)
// 			if(record !== null){
// 				resultDataDriverWorking.push({
// 					driverId: record.get('id'),
// 					dataDriver: {
// 						name: idDriverWorking[i].name,
// 						platNomor: idDriverWorking[i].platNomor,
// 						phone: idDriverWorking[i].phone,
// 						profileImageUrl: idDriverWorking[i].profileImageUrl,
// 						lat: record.get('l').get('0'),
// 						lng: record.get('l').get('1')
// 					}
// 				})
// 				resultWorking.push(record)
// 				console.log('=====================================data driver')
// 				console.log(resultDataDriverWorking)
// 			} else {
// 				console.log('=====================================ada driver mangkir')
// 			}
// 		}

// 		} /* jika terdapat record driverInfo */

// let idCustomerRequest = [];
// 		let dataCustomerRequest = [];
// 		let dataRequest = [];

// 		if (models.driverInfo.length > 1) {

// 		models.customerRequest.forEach((value) => {
// 			idCustomerRequest.push({
// 				id: value.get('id'),
// 				driverId: value.get('driverId'),
// 				lat: value.get('l').get('0'),
// 				lng: value.get('l').get('0')
// 			});
// 		})
// 		for (var i = 0; i < idCustomerRequest.length; i++) {
// 			if(idCustomerRequest[i].driverId == resultDataDriverWorking[i].driverId) {
// 				dataRequest.push({
// 					driverId: idCustomerRequest[i].driverId,
// 					customerId: idCustomerRequest[i].id,
// 					dataDriver: {
// 						name: resultDataDriverWorking[i].dataDriver.name,
// 						platNomor: resultDataDriverWorking[i].dataDriver.platNomor,
// 						phone: resultDataDriverWorking[i].dataDriver.phone,
// 						profileImageUrl: resultDataDriverWorking[i].dataDriver.profileImageUrl,
// 						lat: resultDataDriverWorking[i].dataDriver.lat,
// 						lng: resultDataDriverWorking[i].dataDriver.lng
// 					},
// 					dataCustomer: {
// 						lat: idCustomerRequest[i].lat,
// 						lng: idCustomerRequest[i].lng
// 					}
// 				})
// 			} else {
// 				console.log('===================================== TIDAK');
// 			}
// 		}

// 		} /*jika ada driver info*/

// 		controller.set('dataRequest', dataRequest );
// 		console.log('===================================== ADA');
// 		console.log(dataRequest)
