import Route from '@ember/routing/route'

export default Route.extend({  
  beforeModel: function() {
    return this.get('session').fetch().catch(function() {
    });
  },

  actions: {
    loading(transition, route) {
      let controller = this.controllerFor('application');
      controller.set('currentlyLoading', true);

      transition.finally(function() {
        controller.set('currentlyLoading', false);
      });
    },

    signOut() {
      this.get('session').close()
      .then(() => {
        console.log("=== User logout ===")
        this.transitionTo('/') 
      });
    },
},
  }); 