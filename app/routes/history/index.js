import Route from '@ember/routing/route';
import Authenticated from '../../mixins/auth-guard';
import { hash } from 'rsvp';

export default Route.extend(Authenticated, {
	model() {
		let currentUser = this.get('session.currentUser.uid');
		return hash({
			history: this.get('store').findAll('history').then(data => {
				return data.filterBy('providerId', currentUser)
			}),
			drivers: this.get('store').find('provider', currentUser).then(driver => {
				return driver.get('drivers').then(() => driver); 
			}),
			historyDriver: this.get('store').findAll('history').then(data => {
				return data.filterBy('providerId', currentUser)
			}),
		});
	}, 

	setupController(controller, models) {
			
		// models.drivers.drivers.forEach((value) => {
			// console.log('==========================driver');
			// console.log(value.history);
			// console.log(value.get('driverId'));
		// });
			// // console.log('==========================driver');
			// console.log('count',models.drivers.drivers.length);

		controller.set('data', models.historyDriver);
		controller.set('datas', models.drivers.drivers);

	}
});
