import Route from '@ember/routing/route';
import Authenticated from '../../mixins/auth-guard';
export default Route.extend(Authenticated, {
	model(params) {
		return Ember.RSVP.hash({
			dataHistory: this.get('store').find('history', params.history_id),
		})
	},

	gMap: Ember.inject.service(),

	setupController(controller, models) {
		
		let lat = models.dataHistory.location.to.lat;
		let lng = models.dataHistory.location.to.lng;		
 
		this.get('gMap')
	      .geocode({lat, lng})
	      .then((geocodes) => {
	        // Get first geocode address
	        controller.set('addressCustomer', geocodes[0].formatted_address);
	        console.log('====================================geocode');
	        console.log(geocodes[0].formatted_address);
	      })
	      .catch((err) => console.error(err));

		const driverLoc = { lat: parseFloat(models.dataHistory.location.from.lat), lng: parseFloat(models.dataHistory.location.from.lng) };
		const customerLoc = { lat: parseFloat(models.dataHistory.location.to.lat), lng: parseFloat(models.dataHistory.location.to.lng) };
		
		/**
		*	Menampilkan timestamp 
		*/
		let timestamp = models.dataHistory.timestamp*1000;
		console.log("======================================data data");
		console.log(timestamp);


		let lightMapStyle = [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]}];
		controller.setProperties({ 
	      lightMapStyle: lightMapStyle,
	      customer: customerLoc,
	      driver: driverLoc,
	      dataHistory: models.dataHistory,
	      timestamp: timestamp
	    });

	}
});
