import Route from '@ember/routing/route';
import Authenticated from '../../mixins/auth-guard';
import { hash } from 'rsvp';

export default Route.extend(Authenticated, {
	model(params) {
		return hash({
			driver: this.get('store').find('driver', params.driver_id),
			history: this.get('store').findAll('history')
		})
	},
	
	setupController(controller, models) {
		/**
		 *	Mendapatkan detail pelayanan ambulans berdasarkan driverId
		 */
		let idHistory = [];
		let history = [];
		let dataHistory = [];
		models.driver.history.forEach((value) => {
			idHistory.push(value.get('id'))
		});
		models.history.forEach((value) => {
			history.push({
				id: value.get('id'),
				timestamp: value.get('timestamp')*1000,
			})
		})

		let result = history.filter(function (item) {
			return idHistory.indexOf(item.id) !== -1
		});

		// console.log(result);
		// for (var i = 0; i < history.length; i++) {
		// 	if (history[i].id == idHistory[i]) {
		// 		console.log('Data history');
		// 		// dataHistory.push({
		// 		// 	id: idHistory[i],
		// 		// 	timestamp: history[i].timestamp
		// 		// })
		// 	}
		// }
		
		controller.set('dataHistory', result);
		// 1534089765229 => Ayg
		controller.set('driver', models.driver);
	}
});
