import Route from '@ember/routing/route';
import isLogin from '../mixins/auth-session';

export default Route.extend(isLogin, {  
  // beforeModel() {
  //   return this.get('session').fetch().catch(function() {
  //   });
  // },

  model() {
    return this.store.createRecord('provider');
  },

  afterModel(model, transition) {
    console.log('masih loading')
  },

  firebaseApp : Ember.inject.service(),
  

  actions: {

    signUp(){ 
      let email = this.get('controller').get('model.email');
      let password = this.get('controller').get('model.password');
      let name = this.get('controller').get('model.name');
      let address = this.get('controller').get('model.address');
      let phone = this.get('controller').get('model.phone');
      this.get('firebaseApp').auth()
        .createUserWithEmailAndPassword(email, password)
        .then((userData) => {
          console.log("user berhasil didaftarkan")
          let newUser = this.store.createRecord('provider', {
            // providers: pushObject({
              id: userData.uid,
              status: 'provider',
              approval: false,
              email: userData.email,
              name: name,
              address: address,
              phone: phone,
              password: password,
              created_at: new Date().getTime()
            // })
          });
          newUser.save().then(() => {
            console.log('sign in user');
            this.get('session').open('firebase', {
              provider: 'password',
              email: email,
              password: password
            })
              .then(function() {
                this.get('router').transitionTo('provider');
              }.bind(this))
          })
          .catch((error) => {
              console.log('!!! error create user database')
              console.log(error.message)
            })
        })
        .catch((error) =>{
         console.log('!!! error saat pendaftaran');
         this.get('flashMessages').danger('Something wrongs! '+ error.message, {destroyOnClick: true, sticky: true});
        })
    },

    signIn() {
      this.get('session').open('firebase', {
        // provider: provider,
        provider: 'password',
        email    : this.get('controller').get('providerEmail'),
        password : this.get('controller').get('providerPassword'),
      }).then(function() {
        this.get('router').transitionTo('admin')
      }.bind(this));
    },

    signOut() {
      this.get('session').close()
      .then(() => {
        this.transitionTo('/') 
        console.log("=== User logout ===")
        // this.get('router').transitionTo('/')
      });
    },
},
}); 