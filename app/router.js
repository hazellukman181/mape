import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('admin', function() {
    this.route('login');
    this.route('dashboard', function() {});
    this.route('provider', function() {
      this.route('detail', { path: '/:provider_id/detail' });
    });
    this.route('info', function() {});
    this.route('ambulance', function() {
      this.route('detail', { path: '/:driver_id/detail' });
      this.route('show', { path: '/:history_id/show' });
    });
    this.route('ambulance-active', function() {});
    this.route('customer', function() {
      this.route('detail', { path: '/:customer_id/detail' });
      this.route('show', { path: '/:history_id/show' });
    });

    this.route('page', function() {
      this.route('user-information', function() {
        this.route('about');
        this.route('service');
        this.route('provider');
        this.route('driver');
        this.route('customer');
      });
      this.route('policy', function() {
        this.route('privacy-policy');
        this.route('rule');
      });
    });
    this.route('blog', function() {
      this.route('create');
      this.route('edit', { path: '/:blog_id/edit' });
      this.route('show', { path: '/:blog_id/detail' });
      this.route('comment', { path: '/:blog_id/comment' });
    });
    this.route('driver-working', function() {
      this.route('detail', { path: '/:customer_id/detail'});
    });
  });

  this.route('auth', function() {
    this.route('signIn');
  });
  this.route('provider', function() {});
  this.route('driver', function() {
    this.route('create');
    this.route('after-taste');
    this.route('detail', { path: '/:driver_id/detail' });
    this.route('edit', { path: '/:driver_id/edit' });
  });
  this.route('active-ambulance', function() {
    this.route('detail', { path: '/:driver_id/detail' });
  });
  this.route('driver-working', function() {
    this.route('detail', { path: '/:customer_id/detail'});
  });
  // this.route('detail', { path: '/:driver_id/detail' });
  this.route('history', function() {
    this.route('detail', { path: '/:driver_id/detail'});
    this.route('show', { path: '/:history_id/detail-history'});
  });

  this.route('sidebar-provider');
  this.route('actigo', function() {});
  this.route('policy', function() {});
  this.route('user-information', function() {});
  this.route('rule', function() {});
  this.route('blog', function() {
    this.route('show', { path: '/:blog_id/detail'});
  });
  this.route('404');
  this.route('undefined-route', { path:'/*path' });
});

export default Router;
