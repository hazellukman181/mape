import Controller from '@ember/controller';

export default Controller.extend({
	model() {
		let currentUser = this.get('session.currentUser.uid');
		return Ember.RSVP.hash({
			drivers: this.get('store').find('provider', currentUser).then(driver => {
				return driver.get('drivers').then(() => driver); 
			}),
			driverInfo: this.get('store').find('provider', currentUser).then(driver => {
			    return driver.get('drivers');
			}),
		}) 
	},

	driverActive: 'walla'
});
