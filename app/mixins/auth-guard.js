import Mixin from '@ember/object/mixin';
import { inject as service } from '@ember/service'
export default Mixin.create({
	session: service(),
	beforeModel(){
		if(!this.get('session.currentUser')) {
		 // swal("Anda tidak punya akses", "Anda tidak punya akses ke halaman ini", "warning")
		 //  .then(() => {
			this.transitionTo('/')
		  // })
		}
	}
});