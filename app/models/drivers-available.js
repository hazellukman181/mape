import DS from 'ember-data';

export default DS.Model.extend({
	g: DS.attr('string'),
	l: DS.attr(),
	providerId: DS.attr('string')
});