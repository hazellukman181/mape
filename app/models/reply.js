import DS from 'ember-data';

export default DS.Model.extend({
	commentId: DS.belongsTo('comment', {async: true}),
	body: DS.attr('string'),
	name: DS.attr('string'),
	createdAt: DS.attr('number')
});
