import DS from 'ember-data';
import { match, gte, and, not, lte, or} from '@ember/object/computed';
import { empty } from '@ember/object/computed';

export default DS.Model.extend({
	created_at: DS.attr('number'),
	name: DS.attr('string'),
	phone: DS.attr('string'),
	platNomor: DS.attr('string'),
	code: DS.attr('number'),
	address: DS.attr('string'),
	email: DS.attr('string'),
	profileImageUrl: DS.attr('string'),
	providerId: DS.belongsTo('provider', {async: true}),
	history: DS.hasMany('history', {async: true}), 

	isNameMin: gte('name.length', 5),
	isNameMax: lte('name.length', 20),
	isNameLength: and('isNameMax', 'isNameMin'),
	isAddressLength: gte('address.length', 10),
	isPlatOneDigit: match('platNomor', /^([A-Z])\s[0-9]{1,4}\s([A-Z]){1,2}$/),
	isPlatTwoDigit: match('platNomor', /^([A-Z])([A-Z])\s\d\d\d\d\s([A-Z])([A-Z])+$/),
	isPlatThreeDigit: match('platNomor', /^([A-Z])\s[0-9]{1,4}\s([A-Z]){1,3}$/),
	isPlat: or('isPlatOneDigit', 'isPlatTwoDigit', 'isPlatThreeDigit'),
	isPhoneLongEnough: gte('phone.length', 11), 
	isValidPhone: match('phone', /^(\+[0-9]{1,3}|0)[0-9]{3}( ){0,1}[0-9]{7,8}\b/),
	isValidEmail: match('email', /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/),
	isValid: and('isPhoneLongEnough', 'isValidEmail', 'isPlat', 'isAddressLength', 'isNameLength', 'isValidPhone'),
	isDisabled: not('isValid'),
});