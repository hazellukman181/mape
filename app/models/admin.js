import DS from 'ember-data';
import { match, gte, and, not } from '@ember/object/computed';

export default DS.Model.extend({
	name: DS.attr('string'),
	email: DS.attr(),
	phone: DS.attr('string'),
	status: DS.attr('string'),
	address: DS.attr('string'),
	created_at: DS.attr('string'),
	profileImageUrl: DS.attr('string'),

	// Admin sign in validation
	isValidEmail: match('email', /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/),
	isValidPassword: gte('password.length', 5),
	isValidSignIn: and('isValidEmail', 'isValidPassword'),
	isDisabledSignIn: not('isValidSignIn'),
});