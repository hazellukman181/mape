import DS from 'ember-data';
import { empty } from '@ember/object/computed';

export default DS.Model.extend({
	body: DS.attr('string')
});
