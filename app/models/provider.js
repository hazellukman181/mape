import DS from 'ember-data';
import { match, gte, and, not } from '@ember/object/computed';

export default DS.Model.extend({
	provider_id: DS.attr('string'),
	status: DS.attr('string'),
	name: DS.attr('string'),
	address: DS.attr('string'),
	phone: DS.attr('number'),
	password: DS.attr('string'),
	email: DS.attr('string'),
	created_at: DS.attr('number'),
	approval: DS.attr('string'),
	drivers: DS.hasMany('driver', {async: true}),
	// drivers_availables: DS.hasMany('driveravailable', {async: true}),
	// drivers: DS.hasMany('driver', {async: true}),

	// Form validation
	isProviderNameEnoughLong: gte('name.length', 5),
	isProviderNameValid: match('name', /^[A-Za-z\s]{1,}[\.]{0,1}[A-Za-z\s]{0,}$/),
	isValidName: and('isProviderNameEnoughLong', 'isProviderNameValid'),
	isProviderAddressEnoughLong: gte('address.length', 10),
	isProviderPhoneEnoughLong: gte('phone.length', 7),
	isValidEmail: match('email', /^.+@.+\..+$/),
	isValidPassword: gte('password.length', 5),

	// Instansi sign up
	isValid: and('isProviderNameEnoughLong', 'isProviderAddressEnoughLong', 'isProviderPhoneEnoughLong', 'isValidEmail', 'isValidPassword'),
	isDisabled: not('isValid'),
	// Instansi sign in
	isValidSignIn: and('isValidEmail', 'isValidPassword'),
	isDisabledSignIn: not('isValidSignIn'),
});