import DS from 'ember-data';

export default DS.Model.extend({
	name: DS.attr('string'),
	history: DS.hasMany('history', {async: true}),
	address: DS.attr('string'),
	phone: DS.attr('string'),
	profileImageUrl: DS.attr('string'),
});