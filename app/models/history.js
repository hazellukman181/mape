import DS from 'ember-data';

export default DS.Model.extend({
	driver: DS.belongsTo('driver', {async: true}),
	customer: DS.belongsTo('customer', {async: true}),
	destination: DS.attr('string'),
	distance: DS.attr('string', {async: true}),
	location: DS.attr(),
	// rating: DS.attr('number', {async: true}),
	timestamp: DS.attr('number'),
	providerId: DS.attr('string') 
});