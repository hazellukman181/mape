import DS from 'ember-data';

export default DS.Model.extend({
	comments: DS.hasMany('comment', {async: true}),
	body: DS.attr('string'),
	createdAt: DS.attr('number'),
	updatedAt: DS.attr('number'),
	headerImage: DS.attr('string'),
	title: DS.attr('string'),
	status: DS.attr('string'),
	owner: DS.belongsTo('admin'),
	slug: DS.attr('string'),
	applause: DS.attr('number'),

});
