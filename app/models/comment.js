import DS from 'ember-data';

export default DS.Model.extend({
	blogId: DS.belongsTo('blog', {async: true}),
	body: DS.attr('string'),
	name: DS.attr('string'),
	createdAt: DS.attr('number'),
	replies: DS.hasMany('reply', {async: true}),

	isReply: false
});
